
# Exemple de SDL2 et imGUI ensemble


## SDL2_Simple
Ce code est un exemple minimaliste de code C++ pour illustrer l'usage de SDL2 avec "Dear imGUI".

## imGUI
Dear ImGui is a bloat-free graphical user interface library for C++.
https://github.com/ocornut/imgui


# git 
Ce code fait partie du dépôt git de l'UE "Conception et Développement d'Applications"
https://forge.univ-lyon1.fr/Alexandre.Meyer/L2_ConceptionDevApp



# Installation

[Voir les explications ici](../doc/install.md).


# Compilation

* `cd SDL2_IMGUI_Simple`
* `mkdir build`
* `cd build`
* `make`



# Usage

=> produit 2 exécutables
`./simple`
`./demo`

Vos devriez voir une fenêtre SDL s'ouvrir en affichage un pacman et une ihm imgui.



