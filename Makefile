CXX = g++
CXXFLAGS = -std=c++11 -g
TARGET = mainTest
EXE_TARGET = exemple
OBJS = src/mainTest.o src/Pixel.o src/Image.o
EXE_OBJS = src/mainExemple.o src/Pixel.o src/Image.o
BIN_DIR = bin

all: $(TARGET) $(EXE_TARGET)

$(TARGET): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(TARGET) $(OBJS)

$(EXE_TARGET): $(EXE_OBJS)
	$(CXX) $(CXXFLAGS) -o $(BIN_DIR)/$(EXE_TARGET) $(EXE_OBJS)

src/mainTest.o: src/mainTest.cpp src/Pixel.h
	$(CXX) $(CXXFLAGS) -c src/mainTest.cpp -o src/mainTest.o

src/Pixel.o: src/Pixel.h src/Pixel.cpp
	$(CXX) $(CXXFLAGS) -c src/Pixel.cpp -o src/Pixel.o

src/mainExemple.o: src/mainExemple.cpp src/Pixel.h src/Image.h
	$(CXX) $(CXXFLAGS) -c src/mainExemple.cpp -o src/mainExemple.o

doc: doc/doxyfile
	doxygen doc/doxyfile

clean:
	rm -f $(TARGET) $(EXE_TARGET) $(OBJS) $(BIN_DIR)/$(EXE_OBJS)
