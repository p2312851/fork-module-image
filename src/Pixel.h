#ifndef _PIXEL_H
#define _PIXEL_H
#include <iostream>
#include <fstream>  
#include <string>   
#include <cassert> 

/**
 * @file pixel.h
 * @brief Defintion de la structure Pixel
 */

/**
 * @brief Structure representant un pixel
 */

struct Pixel {
    
    unsigned char r, g, b;
    Pixel() {}
    Pixel(unsigned char nr, unsigned char ng,unsigned char nb) {}
    friend std::ostream& operator<<(std::ostream& os, const Pixel& pixel) {
        os << "(" << static_cast<int>(pixel.r) << ", " << static_cast<int>(pixel.g) << ", " << static_cast<int>(pixel.b) << ")";
        return os;
     }
};

#endif 