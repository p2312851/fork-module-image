#ifndef _IMAGE_H
#define _IMAGE_H

#include "Pixel.h"

/**
 * @file image.h
 * @brief Definition de la classe Image
 */

/**
 * @class Image
 * @brief Classe representant une image
 */

class Image{
    private:
        Pixel* tab;
        int dimx, dimy;
    public:

    /**
     * @brief Constructeur par defaut de la classe Image
     */ 

    // Constructeur de la classe : initialise dimx et dimy à 0
    // n’alloue aucune mémoire pour le tableau de pixel
    Image();

    // Constructeur de la classe : initialise dimx et dimy (après vérification)
    // puis alloue le tableau de pixel dans le tas (image noire)
    Image(int dimensionX, int dimensionY);

     /**
     *@brief Destructeur de la classe Image
     */

    // Destructeur de la classe : déallocation de la mémoire du tableau de pixels
    // et mise à jour des champs dimx et dimy à 0
    ~Image();

     /**
     * @brief obtient le pixel a la position specifiee
     * @param x la coordonnee x du pixel
     * @param y la coordonnee y du pixel
     * @return le pixel a la position specifiee
     */

    // Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
    // La formule pour passer d'un tab 2D à un tab 1D est tab[y*dimx+x]
    Pixel& getPix(unsigned int x, unsigned int y);

    // Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
    Pixel getPix(unsigned int x, unsigned int y) const;

    // Modifie le pixel de coordonnées (x,y)
    /**
     * @brief Definit le pixel a la postion specifiee
     * @param x la coordonnee x du pixel
     * @param pixel Le pixel a definir
     */

    void setPix(int x, int y, const Pixel& couleur);

    /**
     * @brief Dessine un rectangle sur l'image
     * @param xmin la coordonnee x du coin superieur gauche du rectangle
     * @param ymin la coordonnee y du coin superieur gauche du rectangle
     * @param xmax la coordonnee x du coin inferieur droit du rectangle
     * @param ymax la coordonnee y du coin inferieur droit du rectangle
     * @param couleur la couleur du rectangle
     */

    // Dessine un rectangle plein de la couleur dans l'image
    void dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel& couleur);

    /**
     * @brief Efface l\image en la remplissant de la couleur specifiee
     * @param couleur la couleur de fond pour effacer l'image
     */

    // Efface l'image en la remplissant de la couleur en paramètre
    void effacer(const Pixel& couleur);

    /**
     * @brief Test de regression pour la classe Image ()
     * @details cette fonction effectue des tests de regression pour s'assurer que la classe Image fonctionne correctement
     */

    // Effectue une série de tests vérifiant que toutes les fonctions fonctionnent
    // et font bien ce qu’elles sont censées faire, ainsi que les données membres de
    // l'objet sont conformes
    static void testRegression();

    /**
    * @brief Sauvegarde l'image dans un fichier.
    * 
    * @param filename Le nom du fichier dans lequel l'image sera sauvegardée.
    */

    void sauver(const std::string &filename)const;

    /**
    * @brief Ouvre une image à partir d'un fichier.
    * 
    * @param filename Le nom du fichier à ouvrir.
    */
    void ouvrir(const std::string &filename);

    /**
    * @brief Affiche l'image dans la console.
    * 
    * Cette fonction affiche l'image actuelle dans la console.
    */

    void afficherConsole();

     

};

#endif // _IMAGE_H