#include "Image.h"
#include <iostream>
#include <fstream>  
#include <string>   
#include <cassert> 

// Constructeur de la classe : initialise dimx et dimy à 0
// n’alloue aucune mémoire pour le tableau de pixel
Image::Image() : tab(nullptr), dimx(0), dimy(0) {
    // Initialisation ici si nécessaire
}

// Constructeur de la classe : initialise dimx et dimy (après vérification)
// puis alloue le tableau de pixel dans le tas (image noire)
Image::Image(int dimensionX, int dimensionY) : tab(nullptr), dimx(0), dimy(0) {
    // Vérification des dimensions
    if (dimensionX > 0 && dimensionY > 0) {
        dimx = dimensionX;
        dimy = dimensionY;

        // Allocation du tableau de pixels dans le tas (image noire)
        tab = new Pixel[dimx * dimy];
        effacer(Pixel());  // Remplir l'image de la couleur noire
    } else {
        std::cerr << "Erreur : Dimensions invalides pour la création de l'image." << std::endl;
    }
}

// Destructeur de la classe : déallocation de la mémoire du tableau de pixels
// et mise à jour des champs dimx et dimy à 0
Image::~Image() {
    delete[] tab;
    tab = nullptr;
    dimx = dimy = 0;
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
Pixel& Image::getPix(unsigned int x, unsigned int y) {
    assert (x<=dimx && y<=dimy);
    return tab[y*dimx+x];
}

// Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
Pixel Image::getPix(unsigned int x, unsigned int y) const{
    assert (x<=dimx && y<=dimy);
    return tab[y*dimx+x];
}

// Modifie le pixel de coordonnées (x,y)
void Image::setPix(int x, int y, const Pixel& couleur) {
    if (x >= 0 && x < dimx && y >= 0 && y < dimy) {
        tab[y * dimx + x] = couleur;
    } else {
        std::cerr << "Erreur : Coordonnées invalides pour la modification du pixel." << std::endl;
    }
}

// Dessine un rectangle plein de la couleur dans l'image
void Image::dessinerRectangle(int Xmin, int Ymin, int Xmax, int Ymax, const Pixel& couleur) {
    for (int y = Ymin; y <= Ymax; ++y) {
        for (int x = Xmin; x <= Xmax; ++x) {
            setPix(x, y, couleur);
        }
    }
}

// Efface l'image en la remplissant de la couleur en paramètre
void Image::effacer(const Pixel& couleur) {
    dessinerRectangle(0, 0, dimx - 1, dimy - 1, couleur);
}

// Effectue une série de tests vérifiant que toutes les fonctions fonctionnent
// et font bien ce qu’elles sont censées faire, ainsi que les données membres de
// l'objet sont conformes
void Image::testRegression() {
    // Vos tests de régression ici
    // Assurez-vous de tester toutes les fonctionnalités et les cas limites
    // Utilisez std::cout pour afficher les résultats des tests

    // Exemple de test
    Image img(5, 5);  // Création d'une image de 5x5 pixels
    img.dessinerRectangle(1, 1, 3, 3, Pixel(255, 0, 0));  // Dessin d'un rectangle rouge
    img.effacer(Pixel(0, 255, 0));  // Effacement de l'image avec la couleur verte

    // Affichage du pixel en (2,2)
    std::cout<<"Couleur du pixel en (2,2) : "<< img.getPix(2, 2)<< std::endl;
}
void Image::sauver(const std::string &filename) const
{
    std::ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << std::endl;
    fichier << dimx << " " << dimy << std::endl;
    fichier << "255" << std::endl;
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel pix = getPix(x++, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    std::cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const std::string &filename)
{
    std::ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    std::string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> b >> g;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    fichier.close();
    std::cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    std ::cout << dimx << " " << dimy << std::endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPix(x, y);
            std::cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        std::cout << std::endl;
    }
}
